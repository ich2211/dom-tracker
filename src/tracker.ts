class TrackingEntry {
    private readonly stamp: number;
    private readonly action: string;
    private readonly node: Node;

    constructor(action: string, node: Node) {
        this.stamp = Date.now();
        this.action = action;
        this.node = node;
    }

    private buildNodePath(node: Node | HTMLElement | null): string {
        if (!node) {
            return "";
        }
        let nodePath = `${node.nodeName}`;
        if (node instanceof HTMLElement && node.id) {
            nodePath = `${node.nodeName}#${node.id}`;
        }
        if (!node.parentNode) {
            return nodePath;
        }
        nodePath = `${this.buildNodePath(node.parentElement)} > ${nodePath}`;
        return nodePath;
    }

    public toString() {
        return `${this.stamp} [${this.action}]: ${this.buildNodePath(this.node)}`;
    }
}

class Tracker {
    private mutationObserver: MutationObserver | null = null;
    private trackingEntries: TrackingEntry[] = [];

    constructor() {
        /**
         * Check and set a global guard variable.
         * If this content script is injected into the same page again,
         * it will do nothing next time.
         */
        // @ts-ignore
        if (!window.hasRun) {
            // @ts-ignore
            window.hasRun = true;
            this.initMutationObserver(document.body);
            this.registerCommandListener();
        }
    }

    /**
     * adds mutationobserver to given element
     *
     * @param elementToObserve
     */
    protected initMutationObserver(elementToObserve: Element): void {
        // check for existing observer and clear them
        if (this.mutationObserver) {
            this.mutationObserver.disconnect();
        }
        // create new observer
        this.mutationObserver = new MutationObserver((mutations): void => {
            for (const mutation of mutations) {
                if (mutation.type === "childList") {
                    // child list modifications
                    mutation.addedNodes.forEach((node: Node) => {
                        // handle added nodes
                        this.trackingEntries.push(new TrackingEntry('add', node));
                    });
                    mutation.removedNodes.forEach((node: Node) => {
                        // handle added nodes
                        this.trackingEntries.push(new TrackingEntry('remove', node));
                    });
                } else if (mutation.type === "attributes") {
                    // attribute modifications
                    this.trackingEntries.push(new TrackingEntry('change', mutation.target));
                }
            }
        });
        // define config
        const config = { attributes: true, childList: true, subtree: true };
        // start observer
        this.mutationObserver.observe(elementToObserve, config);
    }

    public registerCommandListener() {
        /**
         * Listen for messages from the background script.
         */
        // @ts-ignore
        browser.runtime.onMessage.addListener((message: unknown) => {

            // @ts-ignore
            if (message.command === "export") {
                let data: string[] = [];
                const filename = "dom-changes.txt";
                this.trackingEntries.forEach((entry: TrackingEntry) => {
                    data.push(`${entry.toString()};`);
                });
                const blob = new Blob(data, {type: 'text/csv'});
                if(window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, filename);
                }
                else{
                    var elem = window.document.createElement('a');
                    elem.href = window.URL.createObjectURL(blob);
                    elem.download = filename;
                    document.body.appendChild(elem);
                    elem.click();
                    document.body.removeChild(elem);
                }
            }
            // @ts-ignore
            if (message.command === "reset") {
                this.trackingEntries = [];
            }
        });
    }
}

new Tracker();