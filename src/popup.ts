const exportButton = document.querySelector('.export-action');
const resetButton = document.querySelector('.reset-action');

function getActiveTab() {
    // @ts-ignore
    return browser.tabs.query({active: true, currentWindow: true});
}

// @ts-ignore
resetButton.onclick = function() {
    // @ts-ignore
    getActiveTab().then((tabs) => {
        // @ts-ignore
        browser.tabs.sendMessage(tabs[0].id, {command: "reset"});
    });
};

// @ts-ignore
exportButton.onclick = function() {
    // @ts-ignore
    getActiveTab().then((tabs) => {
        // @ts-ignore
        browser.tabs.sendMessage(tabs[0].id, {command: "export"});
    });
};